//
//  ViewController.swift
//  Stop-Watch
//
//  Created by Click Labs on 1/16/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var timerLabel: UILabel!
    
    //labels variable use to show LAp time...
    @IBOutlet weak var lapLabel1: UILabel!
    @IBOutlet weak var lapLabel2: UILabel!
    

    
    var startTime = NSTimeInterval()
    
    //counter variable for minute, second, milisecond...
    var miliSecCounter = 00
    var secondCounter = 00
    var minuteCounter = 00
    
    //timer variable...
    var timer = NSTimer()
    
    //function for update time...
    func updateTime()
    {
        
        //increment  milisecond counter...
        miliSecCounter++
        
        
        //check if milisecond iis less than 100, if equal to 100 then...
        if miliSecCounter % 10 == 0{
            
            //increment second counter...
            secondCounter++
            
            //when milisecond to 100 then set to 0...
            miliSecCounter = 0
        }
        //check if second is less than 60, if equal to 60 then...
        if  secondCounter % 60 == 0 && secondCounter != 0 {
            
            //increment minute counter...
            minuteCounter++
            
            //set second counter to 0...
            secondCounter = 0
           
        }
        //if this condition true then invalidate the stopwatch...
        if miliSecCounter == 99 && secondCounter == 60 && minuteCounter == 60{
        
            timer.invalidate()
        
        }
        //set the display label to display...
        timerLabel.text = String(minuteCounter) + " :" + String(secondCounter) + ":" + String(miliSecCounter)
        
    }

    //when play button pressed...
    @IBAction func play(sender: AnyObject) {
        
        //this condition is prevent multiple press of play botton...
        if !timer.valid {
            
            //selector constant...
            let aSelector : Selector = "updateTime"
            
            //assign the updated time to timer variable...
            timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: aSelector, userInfo: nil, repeats: true)
            startTime = NSDate.timeIntervalSinceReferenceDate()
        }
        
    }
    //when pause button pressed...
    @IBAction func pause(sender: AnyObject) {
        timer.invalidate()
    }
    //when reset button pressed...
    @IBAction func reset(sender: AnyObject) {
       
        timer.invalidate()
        
        //hide the LAP...
        lapLabel1.hidden = true
        lapLabel2.hidden = true
        
        //reset the label to 00:00:00...
        timerLabel.text = "00:00:00"
        
        miliSecCounter = 0
        secondCounter = 0
        minuteCounter = 0

    }
    
    var isLap = false
    var countPress = 1
    
    @IBAction func lapButton(sender: AnyObject) {
        
        //var isLap = true
       
        if countPress == 1 {
            
            lapLabel1.hidden = false
            lapLabel1.text = timerLabel.text
           // var isLap = false
            println("it Works with label1")
            lapLabel2.hidden = true
        }
        if countPress == 2 {
            lapLabel1.hidden = false
            lapLabel2.hidden = false
            lapLabel2.text = timerLabel.text
           // var isLap = false
            println("it Works with label 2")
        }
        
         countPress++
        if countPress == 3 {
            countPress = 1
        }
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

